import React from 'react'
import styles from '../styles/Home.module.css'
import Filter from '../components/filter'
import Expand from '../components/expand'
import Header from '../components/header'
import Metadata from '../components/head'
import UserList from '../components/userList'
import { getUsersData } from '../lib/usersQuery'

// Get Users from Database
export function getStaticProps() {
  const data = getUsersData();
  return {
    props: {
      data
    }
  }
}

export default function Home({ data }) {
  //States
  const [users, setUsers] = React.useState(data);
  const [filteredUsers, setFilteredUsers] = React.useState([]);
  const [animals, setAnimals] = React.useState([]);
  const [maxUsers, setMaxUsers] = React.useState(10);

  //Extract all animals from the users and set animals into the state
  React.useEffect(() => {
    const animalsFromUsers = users.map(user => user.animals)
    const allAnimals = Array.from(new Set(animalsFromUsers.flat(1)));
    setAnimals(allAnimals)
  }, [])

  // Sort and filter Users by active status 
  const sortAndIsActiveUsers = $users => {
    const sortedUsers = $users.sort(function (a, b) {
      if (a.points < b.points) {
        return 1;
      }
      if (a.points > b.points) {
        return -1;
      }
      return 0;
    });
    const filteredUsers = sortedUsers.filter( user => user.isActive);
    return filteredUsers
  }

  // Sort and filter Users when page is reloaded
  React.useEffect(() => {
    const $users = sortAndIsActiveUsers(users)
    setUsers($users);
    setFilteredUsers($users);
  }, [])

  // Filter Users by animal selected
  const filterUserByAnimal = button => {
    const filteredUsers = users.filter( user => user.animals.includes(button.value));
    setFilteredUsers(filteredUsers);
  }

  // Expand users showed to 25 max
  const showMoreUsers = () => {
    setMaxUsers(25)
  }

  // Collapse users showed to 10 max
  const showLessUsers = () => {
    setMaxUsers(10)
  }

  return (
    <div className={styles.container}>
      <Metadata />
      <Header />
      <Filter 
        animals={animals}
        filterUserByAnimal={filterUserByAnimal}
      />
      <UserList
        filteredUsers={filteredUsers}
        maxUsers={maxUsers}
      />
      <Expand 
        maxUsers={maxUsers}
        showMoreUsers={showMoreUsers}
        showLessUsers={showLessUsers}
      />
    </div>
  )
}
