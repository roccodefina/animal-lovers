import users from '../../lib/users.json';

export default (req, res) => {
  res.status(200).json({ users })
}
