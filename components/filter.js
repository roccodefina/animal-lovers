import styles from '../styles/Home.module.css'

export default function Filter({ animals, filterUserByAnimal }) {
  return (
    <div className={styles.filter}>
      <h5 className={styles.filterTitle}>Choose your favorite animal...</h5>
      <ul className={styles.filterList}>
        {animals.map((animal, index) => (
          <li  key={index}>
            <button className={styles.buttonStyle} key={index} onClick={event => filterUserByAnimal(event.currentTarget)} value={animal}>{animal}</button>
          </li>
        ))}
      </ul>
    </div>
  )
}