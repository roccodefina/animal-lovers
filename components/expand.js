import styles from '../styles/Home.module.css'

export default function Expand({ maxUsers, showMoreUsers, showLessUsers }) {
  return (
    <div className={styles.expandStyle}>
      {maxUsers === 10 ? 
        <button className={styles.buttonStyle} onClick={showMoreUsers}>Show more!!</button> :
        <button className={styles.buttonStyle} onClick={showLessUsers}>Show less!!</button>
      }
    </div>
  )
}