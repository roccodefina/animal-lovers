import styles from '../styles/Home.module.css'

export default function Post({ user }) {
  return (
    <div className={user.isActive ? styles.cardActive : styles.cardNonActive}>
      <h2>{user.name.given} {user.name.surname} <span className={styles.points}>Points {user.points}</span></h2>
      <h5>Age {user.age}</h5>
      {user.animals.map((animal, index) => <span className={styles.animals} key={index}>{animal} </span>)}
    </div>
  )
}