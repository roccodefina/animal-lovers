import styles from '../styles/Home.module.css';
import Post from './post';

export default function UserList({ filteredUsers, maxUsers }) {
  return (
    <div className={styles.row}>
      {filteredUsers.slice(0, maxUsers).map( (user, index) => (
        user.isActive ? 
          <Post key={index} user={user}/>
          : null
        )
      )}    
    </div>
  )
}