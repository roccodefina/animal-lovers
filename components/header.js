import styles from '../styles/Home.module.css'

export default function Header() {
  return (
    <div >
      <h2 className={styles.header}>Animal Lovers</h2>
    </div>
  )
}
